# RTCCafe Client

A javascript client for RTCCafe.

# Installation

```
npm install rtc-cafe-client
```

# Getting Started

Serve [samples/simple-chat.html](samples/simple-chat.html) on the same host as a server running an RTCCafe Signalling Server.

This sample includes:

```
const rtcConfig = {
  iceServers: [{
    urls: ['stun:stun.l.google.com:19302'],
  }],
};
```

The constant `rtcConfig` is an `ICE Configuration`. It points WebRTC at a STUN (or TURN) server. A STUN server determines a client's public network information so it can be transmitted to peers via a signalling server, such as RTCCafe Signaller. You do not need an intimate knowledge of ICE Configuration to understand this example, except that the url `stun:stun.l.google.com:19302` points to a publically available stun server.

For instructions on setting up an RTCCafe Signalling Server, see [the README for RTCCafe Signaller Python.](https://gitlab.com/rtc-cafe/rtc-cafe-signaller-python)

# RTCCafeClient

Uses an RTCCafeSignallingClient instance to send messages to the signalling server in order to orchestrate the peer discovery process.
Allows messages to be sent to and received from peers.

## Properties

### apiVersion

The RTCCafe Signalling API version.

### rtcConfig

The ICE configuration used to configure the WebRTC connections.

### sid

The session id. This is acquired from the signalling server. This will change if the signalling server connection is restarted. Initially, set to "unassigned".

### permanentId

An id permanently assigned to this RTCCafeClient instance that peers will identify this client by.

### connectedPeers

An array of the sids of peers connected over WebRTC.

### discoveredPeers

An array of the sids of peers that are discovered but not necessarily connected over WebRTC.

### state

The state of the client.

- `setup`: Not yet connected to the signalling server
- `connected`: Connected to the signalling server
- `reconnecting`: Disconnected from the signalling server and awaiting reconnection
- `closed`: Signalling server disconnected with `RTCCafeSignallingClient.disconnect` method

## Methods

### constructor

- `rtcConfig`: A configuration to pass into `RTCPeerConnection` to form a peer to peer connection
- `options`:
  - `apiVersion`: The RTCCafe Signalling API version to use (default: `LATEST_API_VERSION`)
  - `signaller`: An instance of `RTCCafeSignallingClient`. If none is specified, a new instance will be created.
  - `enableWebRTCHandshake`: A boolean determining if the initial connection negotiation for peers can happen over WebRTC through mutual peers. (default: true)

### joinRoom

Join the room with the given id.

- Returns: `null`
- `room`: A number or string representing the id of the room to join

### on

Register an event handler for the given event type.

- Returns: `null`
- `eventName`: The name of the event to register the handler for
- `handler`: The handler to register to the event

### off

Detaches the specified event handler for the given event type.

- Returns: `null`
- `eventName`: The name of the event handler to detach the handler from. Set to `null` to remove all event handlers
- `handler`: The handler to detach. Set to null to remove all event handlers for the given `eventName`

### asnyc triggerHandler

Triggers all event handlers for the given event type.

- Returns: `Promise`
- `name`: Name of the event
- `...args`: The arguments to pass to the event handler

### sendMessage

Sends a message.

- Returns: `null`
- `message`: A JSON serializable object to send to peers
- `sid`: The session id of the recipient. If not specified, the message will be sent to all peers.

### addTrack

Makes a media track discoverable by peers.

- Returns: The `id` of the media track
- `track`: A `MediaStreamTrack` object, typically representing streaming video from a webcam or screenshare or streaming audio from a microphone
- `description`: An optional field representing a description of the stream to send to peers. (Default: "no description")

### removeTrack

Removes a media track.

- Returns: The `id` of the media track that was removed, or `null` if none exists.
- `id`: The `id` of the track to remove.

### getTracks

Gets local media tracks along with their definitions.

- Returns: An object of form `[{ id, description, kind, track }, ...]`
- `idFilter`: An array of track ids of tracks to include in the result

### getPeerTrackDefinitions

Gets descriptions of peer media tracks.

- Returns: An object of form `{[peerSid]: {[mediaTrackId]: { id, description, kind }, ...}, ...}`
- `sidFilter`: An array of sids of peers whose tracks to include in the result
- `idFilter`: An array of track ids of tracks to include in the result

### getPeerTracks

Gets peer media tracks.

- Returns: An object of form `{[peerSid]: {[mediaTrackId]: track, ...}, ...}`
- `sidFilter`: An array of sids of peers whose tracks to include in the result
- `idFilter`: An array of track ids of tracks to include in the result

### requestPeerTrack

Requests that the peer streams a track over the WebRTC connection.

- Returns: `null`
- `sid`: The sid of the peer to request the track from
- `id`: The id of the track to request

### removePeerTrack

Removes a peer track from the WebRTC connection.

- Returns: The `id` of the media track that was removed, or `null` if none exists.
- `sid`: The sid of the peer whose track to remove.
- `id`: The id of the track to remove.


## Events

### message

Triggered when a message is received from a peer

- `sender`: The sender of the message
- `message`: The payload of the message

### connectToPeer

Triggered when a new peer is connected to.

- `sid`: The sid of the new peer

### acquireSid

Triggered when client gets a session id from the signalling server.

- `sid`: The sid from the signalling server

### signallerStateChanged

Triggered when the state of the RTCCafeSignallingClient instance changes.

- `state`: The new state of the RTCCafeSignallingClient instance

### disconnectFromPeer

Triggered when a peer disconnects.

- `sid`: The sid of the disconnected peer

### reassignPeerSid

Triggered when a peer changes its sid. This usually happens when they become disconnected from the signalling server
and then reconnect.

- `oldSid`: The old sid of the peer
- `sid`: The new sid of the peer

### peerTrackDefined

Triggered when a peer introduces one of it's discoverable media tracks to the client.

- `sid`: The sid of the peer
- `definition`: An object of the form `{ id, description, kind }`

### peerTrackIdentified

Triggered when a media track is added to a WebRTC connection and its id is determined

- `sid`: The sid of the peer
- `id`: The id of the track
- `track`: The track

### peerTrackRemoved

Triggered when a media track is removed from a WebRTC connection

- `sid`: The sid of the peer
- `id`: The id of the track

### peerTrackDefinitionRemoved

Triggered when a media track is removed from a WebRTC connection or when it is made non-discoverable by a peer

- `sid`: The sid of the peer
- `id`: The id of the track
- `kind`: The kind of the track ("video", "audio", ...)
- `description`: The description of the track


# RTCCafeSignallingClient

Connects the the signalling server and allows signals to be sent to and received from the signalling server.

## Properties

### apiVersion

The RTCCafe Signalling API version.

### sid

The session id. This is acquired from the signalling server and used to identify this client to its peers. Initially, set to "unassigned".

### signalModifier(signalName, payload)

a function which takes an API signal name (acquire-peers, acquire-peers-response, etc.)
and an API payload and returns a modified API payload.

### state

The state of the client.

- `setup`: Not yet connected to the signalling server
- `connected`: Connected to the signalling server
- `reconnecting`: Disconnected from the signalling server and awaiting reconnection
- `closed`: Signalling server disconnected with `RTCCafeSignallingClient.disconnect` method

## Methods

### constructor

- `options`:
  - `apiVersion`: The RTCCafe Signalling API version to use (default `LATEST_API_VERSION`)
  - `host`: The host to connect to
  - `signalModifier(signalName, payload)`: A function which takes an API signal name
  (acquire-peers, acquire-peers-response, etc.) and an API payload and returns a modified API payload.
  This method is ideal for including extra fields such as CSRF tokens for security. RTCCafe Signaller implementations
  allow for custom validation on signalling messages.
  - `ioOptions`: A dictionary of custom options to pass into the socketio client.

### on

Register an event handler for the given event type.

- Returns: `null`
- `eventName`: The name of the event to register the handler for
- `handler`: The handler to register to the event

### off

Detaches the specified event handler for the given event type.

- Returns: `null`
- `eventName`: The name of the event handler to detach the handler from. Set to `null` to remove all event handlers
- `handler`: The handler to detach. Set to null to remove all event handlers for the given `eventName`

### asnyc triggerHandler

Triggers all event handlers for the given event type.

- Returns: `Promise`
- `name`: Name of the event
- `...args`: The arguments to pass to the event handler

### sendSignal

Sends a signal to the signalling server.

- Returns: `null`
- `signalName`: The name of the signal (acquire-peers, acquire-peers-response, etc.)
- `data`: The payload to send in the signal.

### disconnect

Disconnects from the signalling server.

- Returns: `null`


## Events

### stateChanged

Triggered when the signalling client's state changes

- `state`: The new state

### message

Triggered when a message is received from the signalling server

- `event`: The name of the type of signal that was received
- `...args`: Arguments that depend on what was sent by the signalling server

### message.${event}

Triggered when a message of type `event` is received from the signalling server

- `...args`: Arguments that depend on what was sent by the signallign server

### message.${sid}

Triggered when a message is received from the signalling server from peer with given `sid`

- `event`: The name of the type of signal that was received
- `...args`: Arguments that depend on what was sent by the signallign server

### message.${event}.${sid}

Triggered when a message of type `event` is received from the signalling server from peer with given `sid`

- `...args`: Arguments that depend on what was sent by the signallign server
