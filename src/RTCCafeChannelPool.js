import { OrderedDict } from "./OrderedDict";
import {
  // Channel Events
  RTC_CAFE_CHANNEL_EVENT_MESSAGE,
  // Channel Pool Events
  RTC_CAFE_CHANNEL_POOL_EVENT_MESSAGE,
} from "./constants";

export class RTCCafeChannelPool {
  constructor() {
    this._channels = new OrderedDict();
    this._handlers = {};
    this._channelHandlers = {};
  }

  get channelIds() {
    return this._channels.keys;
  }

  get channels() {
    return this._channels.keys.map((id) => this.get(id));
  }

  get(id) {
    return this._channels[id];
  }

  addChannel(channel) {
    const { remoteId } = channel;
    this._channels.set(remoteId, channel);
    const handler = (...args) =>
      this.triggerHandler(
        RTC_CAFE_CHANNEL_POOL_EVENT_MESSAGE,
        channel.remoteId,
        ...args
      );
    channel.on(RTC_CAFE_CHANNEL_EVENT_MESSAGE, handler);
    this._channelHandlers[remoteId] = handler;
  }

  removeChannel(channel) {
    if (typeof channel === "string") {
      channel = this._channels[channel];
    }
    const handler = this._channelHandlers[channel.remoteId];
    channel.off(RTC_CAFE_CHANNEL_EVENT_MESSAGE, handler);
    delete this._channelHandlers[channel.remoteId];
    this._channels.remove(channel.remoteId);
  }

  removeAllChannels() {
    const { channelIds } = this;
    for (const channelId of channelIds) {
      this.removeChannel(channelId);
    }
  }

  broadcast(...args) {
    for (const channel of this.channels) {
      channel.sendMessage(...args);
    }
  }

  async triggerHandler(name, ...args) {
    let handlers = this._handlers[name] || [];
    handlers = [...handlers];
    await Promise.all(handlers.map((handler) => handler(...args)));
  }

  on(eventName, handler) {
    this._handlers[eventName] = this._handlers[eventName] || [];
    this._handlers[eventName].push(handler);
  }

  off(eventName, handler) {
    if (!eventName) {
      this._handlers = {};
      return;
    }
    const handlers = this._handlers[eventName];
    if (!handlers || !handlers.includes(handler)) {
      return;
    }
    const index = handlers.indexOf(handler);
    handlers.splice(index, 1);
  }
}
