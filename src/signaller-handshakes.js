import {
  SIGNALLER_EVENT_OFFER,
  SIGNALLER_EVENT_ANSWER,
  SIGNALLER_EVENT_CANDIDATE,
  // Channel Events
  RTC_CAFE_CHANNEL_EVENT_NEGOTIATION_NEEDED,
  RTC_CAFE_CHANNEL_EVENT_ICE_CANDIDATE,
  RTC_CAFE_CHANNEL_EVENT_DATA_CHANNEL_OPEN,
  RTC_CAFE_CHANNEL_EVENT_CONNECTION_STATE_CHANGE,
} from "./constants";
import { RTCCafeChannel } from "./RTCCafeChannel";

const handleSignalsFromPeer = (client, peerSid, onConnectionLost, handlers) => {
  const { signaller } = client;

  const handleState = (state, data, ...args) => {
    if (client.state !== "connected") {
      throw new Error(
        `Client is in unexpected state ${client.state}. ` +
          "Signalling server connection may have been lost."
      );
    }

    handlers[state](data, ...args);
  };

  const signalHandlers = {};
  for (const event of Object.keys(handlers)) {
    const handler = (...args) => handleState(event, ...args);
    signaller.on(`message.${event}.${peerSid}`, handler);
    signalHandlers[event] = handler;
  }

  const clientStateChangedHandler = (state) => {
    if (state === "connected") {
      // signaller is still connected. Nothing to do
      return;
    }
    // remove all event handlers
    client.off("signallerStateChanged", clientStateChangedHandler);
    for (const event in signalHandlers) {
      if (!Object.prototype.hasOwnProperty.call(signalHandlers, event)) {
        continue;
      }
      signaller.off(`message.${event}.${peerSid}`, signalHandlers[event]);
    }
    onConnectionLost();
  };

  client.on("signallerStateChanged", clientStateChangedHandler);
};

const createChannelFromOffer = (client, peerId, offerDesc) =>
  new Promise((resolve, reject) => {
    const {
      permanentId,
      signaller,
      rtcConfig,
      getPeerSid,
      apiVersion,
    } = client;
    const boundGetPeerSid = getPeerSid.bind(client);
    const peerSid = boundGetPeerSid(peerId);

    const connection = new RTCPeerConnection(rtcConfig);
    const channel = new RTCCafeChannel(permanentId, peerId, connection);

    channel.handleOffer(offerDesc).then((answer) =>
      signaller.sendSignal(SIGNALLER_EVENT_ANSWER, {
        apiVersion,
        id: permanentId,
        desc: answer,
        recipient: peerSid,
      })
    );

    channel.on(RTC_CAFE_CHANNEL_EVENT_DATA_CHANNEL_OPEN, () => {
      // Turn off all the event handlers we added
      channel.off();
      resolve(channel);
    });

    let handlers;
    switch (apiVersion) {
      case 1:
        handlers = {
          [SIGNALLER_EVENT_CANDIDATE]: ({ candidate }) => {
            candidate.usernameFragment = null;
            channel.addIceCandidate(new RTCIceCandidate(candidate));
          },
        };
        break;
      default:
        throw new Error(
          `Unexpected API version supplied for handshake ${apiVersion}`
        );
    }

    handleSignalsFromPeer(client, peerSid, reject, handlers);
  });

export const initiateHandshakeOverSignaller = (client, peerId) =>
  new Promise((resolve, reject) => {
    const {
      permanentId,
      signaller,
      rtcConfig,
      getPeerSid,
      apiVersion,
    } = client;
    const boundGetPeerSid = getPeerSid.bind(client);
    const peerSid = boundGetPeerSid(peerId);

    const connection = new RTCPeerConnection(rtcConfig);
    const channel = new RTCCafeChannel(permanentId, peerId, connection);

    // send any ice candidates to the other peer
    channel.on(RTC_CAFE_CHANNEL_EVENT_ICE_CANDIDATE, ({ candidate }) => {
      if (!candidate) {
        return;
      }
      signaller.sendSignal(SIGNALLER_EVENT_CANDIDATE, {
        apiVersion,
        candidate,
        recipient: peerSid,
        id: permanentId,
      });
    });

    // Let the "negotiationneeded" event trigger offer generation.
    // This will be triggered when we create the channel
    channel.on(RTC_CAFE_CHANNEL_EVENT_NEGOTIATION_NEEDED, () => {
      channel.createOffer().then((offer) =>
        signaller.sendSignal(SIGNALLER_EVENT_OFFER, {
          apiVersion,
          desc: offer,
          recipient: peerSid,
          id: permanentId,
        })
      );
    });

    channel.on(RTC_CAFE_CHANNEL_EVENT_DATA_CHANNEL_OPEN, () => {
      // Turn off all the event handlers we added
      channel.off();
      resolve(channel);
    });

    channel.on(
      RTC_CAFE_CHANNEL_EVENT_CONNECTION_STATE_CHANGE,
      ({ iceConnectionState }) => {
        if (["failed", "disconnected", "closed"].includes(iceConnectionState)) {
          reject(new Error("iceConnectionLost"));
        }
      }
    );

    channel.createDataChannel();

    let handlers;
    switch (apiVersion) {
      case 1:
        handlers = {
          [SIGNALLER_EVENT_ANSWER]: ({ desc }) => {
            channel.handleAnswer(desc);
          },
        };
        break;
      default:
        throw new Error(
          `Unexpected API version supplied for handshake ${apiVersion}`
        );
    }

    const onConnectionLost = () => reject(new Error("signallerConnectionLost"));

    handleSignalsFromPeer(client, peerSid, onConnectionLost, handlers);
  });

export const handleOffersOverSignaller = (client, setPeerSid, setChannel) => {
  const { apiVersion, signaller } = client;
  let handlers;

  const handleState = (state, data, ...args) => {
    if (client.state !== "connected") {
      throw new Error(
        `Client is in unexpected state ${client.state}. ` +
          "Signalling server connection may have been lost."
      );
    }

    handlers[state](data, ...args);
  };

  switch (apiVersion) {
    case 1:
      handlers = {
        [SIGNALLER_EVENT_OFFER]: ({ desc, id, sid }) => {
          setPeerSid(id, sid);
          createChannelFromOffer(client, id, desc)
            .then(setChannel)
            .catch(() => {});
        },
      };
      break;
    default:
      throw new Error(
        `Unexpected API version supplied for handshake ${apiVersion}`
      );
  }

  const signalHandlers = {};
  for (const event of Object.keys(handlers)) {
    const handler = (...args) => handleState(event, ...args);
    signaller.on(`message.${event}`, handler);
    signalHandlers[event] = handler;
  }
};
