export class OrderedDict {
  constructor() {
    this.length = 0;
    this.keys = [];
  }

  indexOf(key) {
    return this.keys.indexOf(key);
  }

  remove(key) {
    const index = this.indexOf(key);
    if (index === -1) {
      return null;
    }
    this.keys.splice(index, 1);
    const value = this[key];
    delete this[key];
    this.length = this.keys.length;
    return value;
  }

  set(key, value) {
    const index = this.indexOf(key);
    if (index === -1) {
      this.keys.push(key);
      this.length = this.keys.length;
    }
    this[key] = value;
  }
}
