import {
  SIGNALLER_EVENT_ACQUIRE_PEERS,
  SIGNALLER_EVENT_ACQUIRE_PEERS_RESPONSE,
} from "./constants";

export const startPeerDiscovery = (
  client,
  setDiscoveredPeer,
  initiateHandshake
) => {
  const { apiVersion, signaller, room, permanentId } = client;
  let handlers;

  const handleState = (state, ...args) => {
    if (client.state !== "connected") {
      throw new Error(
        `Client is in unexpected state ${client.state}. ` +
          "Signalling server connection may have been lost."
      );
    }

    handlers[state](...args);
  };

  switch (apiVersion) {
    case 1:
      handlers = {
        [SIGNALLER_EVENT_ACQUIRE_PEERS]: ({ id, sid }) => {
          if (client.discoveredPeers.includes(id)) {
            // We've already discovered this peer
            return;
          }
          setDiscoveredPeer(id, sid);
          signaller.sendSignal(SIGNALLER_EVENT_ACQUIRE_PEERS_RESPONSE, {
            apiVersion,
            room,
            id: permanentId,
            recipient: sid,
          });
        },
        [SIGNALLER_EVENT_ACQUIRE_PEERS_RESPONSE]: ({ id, sid }) => {
          setDiscoveredPeer(id, sid);
          initiateHandshake(id);
        },
      };
      break;
    default:
      throw new Error(
        `Unexpected API version supplied for handshake ${apiVersion}`
      );
  }

  const signalHandlers = {};
  for (const event of Object.keys(handlers)) {
    const handler = (...args) => handleState(event, ...args);
    signaller.on(`message.${event}`, handler);
    signalHandlers[event] = handler;
  }

  signaller.sendSignal("join", { apiVersion, room, id: permanentId });

  const clientStateChangedHandler = (state) => {
    if (state === "connected") {
      // signaller is still connected. Nothing to do
      return;
    }
    // remove all event handlers
    client.off("signallerStateChanged", clientStateChangedHandler);
    for (const event in signalHandlers) {
      if (!Object.prototype.hasOwnProperty.call(signalHandlers, event)) {
        continue;
      }
      signaller.off(`message.${event}`, signalHandlers[event]);
    }
  };

  client.on("signallerStateChanged", clientStateChangedHandler);
};
