import { v4 as uuidv4 } from "uuid";

import { OrderedDict } from "./OrderedDict";
import { RTCCafeChannelPool } from "./RTCCafeChannelPool";
import {
  RTC_MSG_TYPE_ROUTER_WRAPPED_MESSAGE,
  RTC_MSG_TYPE_DECLARE_CONNECTION,
  RTC_MSG_TYPE_DECLARE_ALL_CONNECTIONS,
  RTC_MSG_TYPE_ROUTER_ACK,
  RTC_MSG_TYPE_ROUTER_CLIENT_MESSAGE,
  // Channel Events
  RTC_CAFE_CHANNEL_POOL_EVENT_MESSAGE,
  // Router Events
  RTC_CAFE_ROUTER_EVENT_MESSAGE,
  RTC_CAFE_ROUTER_EVENT_REACHABILITY_UPDATED,
  RTC_CAFE_ROUTER_EVENT_REACHABILITY_BULK_UPDATED,
} from "./constants";

const RESEND_DELAY_BY_HOPS = 5000;

const setReachabilityMapEntry = (
  reachabilityMap,
  id,
  nextHopId,
  hops,
  allowIncrease = false
) => {
  reachabilityMap.set(id, reachabilityMap[id] || new OrderedDict());
  const hopsByConduitId = reachabilityMap[id];
  hops =
    !hopsByConduitId.keys.includes(nextHopId) || allowIncrease
      ? hops
      : Math.min(hops, hopsByConduitId[nextHopId]);
  hopsByConduitId.set(nextHopId, hops);
  return hops;
};

const removeReachabilityMapEntry = (router, id, nextHopId) => {
  const { _reachabilityMap, _reliableMessages } = router;
  if (_reachabilityMap.keys.includes(id)) {
    _reachabilityMap[id].remove(nextHopId);
    if (_reachabilityMap[id].keys.length !== 0) {
      // Peer 'id' is still reachable. We're done here
      return;
    }
    // Peer 'id' is no longer reachable, so we have to fail any messages we tried to send it
    if (!_reliableMessages.keys.includes(id)) {
      // We are not awaiting ACKs for any messages from peer 'id'
      return;
    }
    // Trigger failure handlers for all the reliable messages we tried to send to the peer
    const reliableMessagesForSender = this._reliableMessages[id];
    for (const uniqueId of reliableMessagesForSender.keys) {
      reliableMessagesForSender[uniqueId].onFailure();
    }
    _reliableMessages.remove(id);
  }
};

const determineNextHop = (reachabilityMap, destination) => {
  const hopsByConduitId = reachabilityMap[destination] || new OrderedDict();
  let minHops = Infinity;
  let minHopsConduit = null;
  for (const id of hopsByConduitId.keys) {
    const hops = hopsByConduitId[id];
    if (hops < minHops) {
      minHops = hops;
      minHopsConduit = id;
    }
  }
  return [minHopsConduit, minHops];
};

const wrapAndSendMessage = (router, destination, messageType, message) => {
  // Wrap the message in information router instances use to route and ACK messages
  const [nextHop] = determineNextHop(router._reachabilityMap, destination);
  if (!nextHop) {
    // TODO: error handling?
    return false;
  }
  const conduitChannel = router._channelPool.get(nextHop);
  const originalSender = router.routerId;
  message = { destination, originalSender, message, messageType };
  conduitChannel.sendMessage(message);
  return true;
};

const declareConnection = (
  router,
  id,
  hops,
  allowIncrease = false,
  previousHops = []
) => {
  for (const otherId of router._channelPool.channelIds) {
    if (otherId === id || previousHops.includes(otherId)) {
      continue;
    }
    wrapAndSendMessage(router, otherId, RTC_MSG_TYPE_DECLARE_CONNECTION, {
      id,
      hops,
      allowIncrease,
      previousHops,
    });
  }
};

const declareAllConnections = (router, toId) => {
  const { _reachabilityMap } = router;
  const data = {};
  for (const otherId of _reachabilityMap.keys) {
    if (otherId === toId) {
      continue;
    }
    const hopsByConduitId = _reachabilityMap[toId];
    let minHops = Infinity;
    for (const conduitId of hopsByConduitId.keys) {
      minHops = Math.min(hopsByConduitId[conduitId], minHops);
    }
    if (minHops !== Infinity) {
      data[otherId] = minHops;
    }
  }
  wrapAndSendMessage(router, toId, RTC_MSG_TYPE_DECLARE_ALL_CONNECTIONS, data);
};

const handleDeclareConnectionMessage = (router, sender, json) => {
  const { _reachabilityMap, triggerHandler } = router;
  const boundTriggerHandler = triggerHandler.bind(router);
  const { message } = json;
  const { id, hops, allowIncrease, previousHops } = message;
  if (hops === Infinity) {
    // the sender has no direct or indirect connection to `id`
    removeReachabilityMapEntry(router, id, sender);
  } else {
    setReachabilityMapEntry(
      _reachabilityMap,
      id,
      sender,
      hops + 1,
      allowIncrease
    );
  }
  const [nextHopId, minHops] = determineNextHop(_reachabilityMap, id);
  if (hops === minHops) {
    // We've updated the number of hops, so we'll propagate it to our neighbours
    declareConnection(router, id, minHops, allowIncrease, [
      ...previousHops,
      sender,
    ]);
  }
  boundTriggerHandler(
    RTC_CAFE_ROUTER_EVENT_REACHABILITY_UPDATED,
    id,
    nextHopId,
    minHops
  );
};

const handleDeclareAllConnectionsMessage = (router, sender, json) => {
  const { _reachabilityMap, triggerHandler } = router;
  const boundTriggerHandler = triggerHandler.bind(router);
  const { message } = json;
  for (const otherId in message) {
    if (!Object.prototype.hasOwnProperty.call(message, otherId)) {
      continue;
    }
    const hopsFromSender = message[otherId];
    setReachabilityMapEntry(
      _reachabilityMap,
      otherId,
      sender,
      hopsFromSender + 1
    );
  }
  boundTriggerHandler(RTC_CAFE_ROUTER_EVENT_REACHABILITY_BULK_UPDATED, sender);
};

const handleAck = (router, sender, json) => {
  const { _reliableMessages } = router;
  const { originalSender, message } = json;
  const { uniqueId: ackUniqueId } = message;
  if (!_reliableMessages.keys.includes(originalSender)) {
    // TODO: handle getting unrecognized ACK
    return;
  }
  const reliableMessagesForSender = _reliableMessages[originalSender];
  if (!reliableMessagesForSender.keys.includes(ackUniqueId)) {
    // TODO: handle getting unrecognized ACK
    return;
  }
  const { onSuccess } = reliableMessagesForSender[ackUniqueId];
  const boundOnSuccess = onSuccess.bind(router);
  boundOnSuccess();
  reliableMessagesForSender.remove(ackUniqueId);
  if (reliableMessagesForSender.keys.length === 0) {
    _reliableMessages.remove(originalSender);
  }
};

const handleClientMessage = (router, sender, json) => {
  const { triggerHandler } = router;
  const boundTriggerHandler = triggerHandler.bind(router);
  const { originalSender, message } = json;
  const { message: originalMessage } = message;
  boundTriggerHandler(
    RTC_CAFE_ROUTER_EVENT_MESSAGE,
    originalSender,
    sender,
    originalMessage
  );
  boundTriggerHandler(
    `${RTC_CAFE_ROUTER_EVENT_MESSAGE}.originalSender:${originalSender}`,
    sender,
    originalMessage
  );
  boundTriggerHandler(
    `${RTC_CAFE_ROUTER_EVENT_MESSAGE}.sender:${sender}`,
    originalSender,
    originalMessage
  );
  boundTriggerHandler(
    `${RTC_CAFE_ROUTER_EVENT_MESSAGE}.originalSender:${originalSender}.sender:${sender}`,
    originalMessage
  );
};

const forwardMessage = (router, message) => {
  const { destination } = message;
  const [nextHop] = determineNextHop(router._reachabilityMap, destination);
  if (!nextHop) {
    // TODO: error handling?
    return false;
  }
  const conduitChannel = router._channelPool.get(nextHop);
  conduitChannel.sendMessage(message);
  return true;
};

const isValidRouterMessage = ({ messageType }) =>
  [
    RTC_MSG_TYPE_ROUTER_WRAPPED_MESSAGE,
    RTC_MSG_TYPE_DECLARE_CONNECTION,
    RTC_MSG_TYPE_DECLARE_ALL_CONNECTIONS,
    RTC_MSG_TYPE_ROUTER_ACK,
    RTC_MSG_TYPE_ROUTER_CLIENT_MESSAGE,
  ].includes(messageType);

export class RTCCafeRouter {
  constructor(routerId) {
    this.routerId = routerId;
    this._channelPool = new RTCCafeChannelPool();
    this._reachabilityMap = new OrderedDict();
    this._reliableMessages = new OrderedDict();
    this._receivedWrappedMessageIds = {};
    this._handlers = {};

    const handleDirectMessages = (sender, json) => {
      const { originalSender, messageType, message } = json;
      const { uniqueId } = message;
      if (uniqueId && messageType !== RTC_MSG_TYPE_ROUTER_ACK) {
        // Send back an ACK
        wrapAndSendMessage(this, originalSender, RTC_MSG_TYPE_ROUTER_ACK, {
          uniqueId,
        });
      }
      if (messageType === RTC_MSG_TYPE_DECLARE_CONNECTION) {
        handleDeclareConnectionMessage(this, sender, json);
      } else if (messageType === RTC_MSG_TYPE_DECLARE_ALL_CONNECTIONS) {
        handleDeclareAllConnectionsMessage(this, sender, json);
      } else if (messageType === RTC_MSG_TYPE_ROUTER_ACK) {
        handleAck(this, sender, json);
      } else if (messageType === RTC_MSG_TYPE_ROUTER_CLIENT_MESSAGE) {
        handleClientMessage(this, sender, json);
      }
    };

    this._channelPool.on(
      RTC_CAFE_CHANNEL_POOL_EVENT_MESSAGE,
      (sender, json) => {
        if (!isValidRouterMessage(json)) {
          // TODO: error handling?
          return;
        }
        const { destination } = json;
        if (destination === routerId) {
          // This message is meant for us
          handleDirectMessages(sender, json);
        } else {
          // This message isn't meant for us
          forwardMessage(this, json);
        }
      }
    );
  }

  get channelIds() {
    return this._channelPool.channelIds;
  }

  get channels() {
    return this._channelPool.channels;
  }

  get(id) {
    return this._channelPool.get(id);
  }

  addChannel(channel) {
    const { remoteId } = channel;
    // add the channel to the channel pool
    this._channelPool.addChannel(channel);
    setReachabilityMapEntry(this._reachabilityMap, remoteId, remoteId, 1);
    declareConnection(this, remoteId, 1);
    declareAllConnections(this, remoteId);
    // We have a direct connection to this channel
    if (remoteId in this._receivedWrappedMessageIds) {
      // We can ACK all their messages
      for (const uniqueId of this._receivedWrappedMessageIds[remoteId]) {
        wrapAndSendMessage(this, remoteId, RTC_MSG_TYPE_ROUTER_ACK, {
          uniqueId,
        });
      }
    }
    delete this._receivedWrappedMessageIds[remoteId];
    channel.on("dataChannelClose", () => this.removeChannel(channel));
  }

  removeChannel(channel) {
    // Only removes channel from channel pool and reachability map
    // Does not disconnect
    const { remoteId } = channel;
    if (!this.channelIds.includes(remoteId)) {
      return;
    }
    removeReachabilityMapEntry(this, remoteId, remoteId);
    const [nextHopId, minHops] = determineNextHop(
      this._reachabilityMap,
      remoteId
    );
    declareConnection(this, remoteId, minHops, true);
    this.triggerHandler(
      RTC_CAFE_ROUTER_EVENT_REACHABILITY_UPDATED,
      remoteId,
      nextHopId,
      minHops
    );
    this._channelPool.removeChannel(channel);
  }

  removeAllChannels() {
    // Clears channel pool and and reachability map
    // Rejects all "sendMessage" promises
    // Does not send message with new reachability map
    this._channelPool.removeAllChannels();
    this._reachabilityMap = new OrderedDict();
    for (const channelId of this._reliableMessages.keys) {
      const messages = this._reliableMessages[channelId];
      for (const uniqueId of messages.keys) {
        const { onFailure } = messages[uniqueId];
        onFailure();
      }
    }
  }

  broadcast(message) {
    for (const peerId of this.channelIds) {
      wrapAndSendMessage(
        this,
        peerId,
        // All messages from outside this router instance will be marked as a "client message"
        RTC_MSG_TYPE_ROUTER_CLIENT_MESSAGE,
        { message }
      );
    }
  }

  sendMessage(destination, message) {
    const [, hops] = determineNextHop(this._reachabilityMap, destination);
    const reliableMessage = { destination };
    return new Promise((resolve, reject) => {
      if (hops === destination) {
        wrapAndSendMessage(
          this,
          destination,
          // All messages from outside this router instance will be marked as a "client message"
          RTC_MSG_TYPE_ROUTER_CLIENT_MESSAGE,
          { message }
        );
        resolve();
      }
      const uniqueId = uuidv4();
      const sendMessage = () => {
        wrapAndSendMessage(
          this,
          destination,
          // All messages from outside this router instance will be marked as a "client message"
          RTC_MSG_TYPE_ROUTER_CLIENT_MESSAGE,
          {
            uniqueId,
            message,
          }
        );
      };
      const interval = setInterval(sendMessage, hops * RESEND_DELAY_BY_HOPS);
      const buildHandler = (func) => () => {
        clearInterval(interval);
        func();
      };
      reliableMessage.onSuccess = buildHandler(resolve);
      reliableMessage.onFailure = buildHandler(reject);
      if (!this._reliableMessages.keys.includes(destination)) {
        this._reliableMessages.set(destination, new OrderedDict());
      }
      this._reliableMessages[destination].set(uniqueId, reliableMessage);
      sendMessage();
    });
  }

  isPeerReachable(peerId) {
    const [nextHop] = determineNextHop(this._reachabilityMap, peerId);
    return nextHop;
  }

  async triggerHandler(name, ...args) {
    let handlers = this._handlers[name] || [];
    handlers = [...handlers];
    await Promise.all(handlers.map((handler) => handler(...args)));
  }

  on(eventName, handler) {
    this._handlers[eventName] = this._handlers[eventName] || [];
    this._handlers[eventName].push(handler);
  }

  off(eventName, handler) {
    if (!eventName) {
      this._handlers = {};
      return;
    }
    const handlers = this._handlers[eventName];
    if (!handlers || !handlers.includes(handler)) {
      return;
    }
    const index = handlers.indexOf(handler);
    handlers.splice(index, 1);
  }
}
