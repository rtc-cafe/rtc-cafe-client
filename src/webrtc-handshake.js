import {
  RTC_MSG_TYPE_ICE_CANDIDATE,
  RTC_MSG_TYPE_OFFER,
  RTC_MSG_TYPE_ANSWER,
  // Channel Events
  RTC_CAFE_CHANNEL_EVENT_NEGOTIATION_NEEDED,
  RTC_CAFE_CHANNEL_EVENT_CONNECTION_STATE_CHANGE,
  RTC_CAFE_CHANNEL_EVENT_ICE_CANDIDATE,
  RTC_CAFE_CHANNEL_EVENT_DATA_CHANNEL_OPEN,
  // Router Events
  RTC_CAFE_ROUTER_EVENT_MESSAGE,
} from "./constants";
import { RTCCafeChannel } from "./RTCCafeChannel";

const createChannelFromOffer = (client, router, peerId, offerDesc) =>
  new Promise((resolve) => {
    const { rtcConfig, permanentId } = client;

    const connection = new RTCPeerConnection(rtcConfig);
    const channel = new RTCCafeChannel(permanentId, peerId, connection);
    const handler = (sender, json) => {
      const { messageType } = json;
      if (peerId === sender) {
        // Ignore direct messages
        return;
      }
      if (messageType === RTC_MSG_TYPE_ICE_CANDIDATE) {
        const {
          message: { candidate },
        } = json;
        candidate.usernameFragment = null;
        channel.addIceCandidate(new RTCIceCandidate(candidate));
      }
    };

    const eventName = `${RTC_CAFE_ROUTER_EVENT_MESSAGE}.originalSender:${peerId}`;
    router.on(eventName, handler);

    channel.on(RTC_CAFE_CHANNEL_EVENT_DATA_CHANNEL_OPEN, () => {
      // Turn off all the event handlers we added
      channel.off();
      router.off(eventName, handler);
      resolve(channel);
    });

    channel.handleOffer(offerDesc).then((answer) =>
      router.sendMessage(peerId, {
        messageType: RTC_MSG_TYPE_ANSWER,
        message: { desc: answer },
      })
    );
  });

export const handleRenegotiationOverWebRTC = (channel, router) => {
  const peerId = channel.remoteId;
  // Let the "negotiationneeded" event trigger offer generation.
  // This will be triggered when media track is added
  channel.on(RTC_CAFE_CHANNEL_EVENT_NEGOTIATION_NEEDED, () =>
    channel.createOffer().then((offer) => {
      // send the offer to the other peer
      router.sendMessage(peerId, {
        messageType: RTC_MSG_TYPE_OFFER,
        message: { desc: offer },
      });
    })
  );

  // send any ice candidates to the other peer
  channel.on(RTC_CAFE_CHANNEL_EVENT_ICE_CANDIDATE, ({ candidate }) => {
    if (!candidate) {
      return;
    }
    router.sendMessage(peerId, {
      messageType: RTC_MSG_TYPE_ICE_CANDIDATE,
      message: { candidate },
    });
  });

  const eventName = `${RTC_CAFE_ROUTER_EVENT_MESSAGE}.originalSender:${peerId}.sender:${peerId}`;
  router.on(eventName, (json) => {
    const { messageType } = json;
    if (messageType === RTC_MSG_TYPE_OFFER) {
      const {
        message: { desc },
      } = json;
      channel.handleOffer(desc).then((answer) =>
        router.sendMessage(peerId, {
          messageType: RTC_MSG_TYPE_ANSWER,
          message: { desc: answer },
        })
      );
    } else if (messageType === RTC_MSG_TYPE_ANSWER) {
      const {
        message: { desc },
      } = json;
      channel.handleAnswer(desc);
    } else if (messageType === RTC_MSG_TYPE_ICE_CANDIDATE) {
      const {
        message: { candidate },
      } = json;
      candidate.usernameFragment = null;
      channel.addIceCandidate(new RTCIceCandidate(candidate));
    }
  });
};

export const initIntroHandshakeOverWebRTC = (client, router, peerId) =>
  new Promise((resolve, reject) => {
    const { rtcConfig, permanentId } = client;

    const connection = new RTCPeerConnection(rtcConfig);
    const channel = new RTCCafeChannel(permanentId, peerId, connection);
    const handler = (sender, json) => {
      if (peerId === sender) {
        // Ignore direct messages
        return;
      }
      const { messageType } = json;
      if (messageType === RTC_MSG_TYPE_ANSWER) {
        const {
          message: { desc },
        } = json;
        channel.handleAnswer(desc);
      }
    };
    const eventName = `${RTC_CAFE_ROUTER_EVENT_MESSAGE}.originalSender:${peerId}`;
    router.on(eventName, handler);

    // send any ice candidates to the other peer
    channel.on(RTC_CAFE_CHANNEL_EVENT_ICE_CANDIDATE, ({ candidate }) => {
      if (!candidate) {
        return;
      }
      router
        .sendMessage(peerId, {
          messageType: RTC_MSG_TYPE_ICE_CANDIDATE,
          message: {
            candidate,
          },
        })
        .catch(reject);
    });

    // Let the "negotiationneeded" event trigger offer generation.
    // This will be triggered when we create the channel
    channel.on(RTC_CAFE_CHANNEL_EVENT_NEGOTIATION_NEEDED, () => {
      channel.createOffer().then((offer) =>
        router
          .sendMessage(peerId, {
            messageType: RTC_MSG_TYPE_OFFER,
            message: { desc: offer },
          })
          .catch(reject)
      );
    });

    channel.on(RTC_CAFE_CHANNEL_EVENT_DATA_CHANNEL_OPEN, () => {
      // Turn off all the event handlers we added
      channel.off();
      router.off(eventName, handler);
      resolve(channel);
    });

    channel.on(
      RTC_CAFE_CHANNEL_EVENT_CONNECTION_STATE_CHANGE,
      ({ iceConnectionState }) => {
        if (["failed", "disconnected", "closed"].includes(iceConnectionState)) {
          reject();
        }
      }
    );

    channel.createDataChannel();
  });

export const handleOffersOverWebRTC = (client, router, setChannel) => {
  router.on(RTC_CAFE_ROUTER_EVENT_MESSAGE, (originalSender, sender, json) => {
    if (originalSender === sender) {
      // Ignore direct messages
      return;
    }
    const { messageType } = json;
    if (messageType === RTC_MSG_TYPE_OFFER) {
      const {
        message: { desc },
      } = json;
      createChannelFromOffer(client, router, originalSender, desc).then(
        setChannel
      );
    }
  });
};
