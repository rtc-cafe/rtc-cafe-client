import {
  RTC_MSG_TYPE_INTRODUCE_MEDIA_TRACK,
  RTC_MSG_TYPE_REMOVE_MEDIA_TRACK,
  RTC_MSG_TYPE_REQUEST_MEDIA_TRACK,
  RTC_MSG_TYPE_CORRELATE_MEDIA_TRACK,
  // Channel Events
  RTC_CAFE_CHANNEL_EVENT_PEER_TRACK_DEFINED,
  RTC_CAFE_CHANNEL_EVENT_PEER_TRACK_REMOVED,
  RTC_CAFE_CHANNEL_EVENT_PEER_TRACK_DEFINITION_REMOVED,
  RTC_CAFE_CHANNEL_EVENT_PEER_TRACK_IDENTIFIED,
  RTC_CAFE_CHANNEL_EVENT_LOCAL_DESCRIPTION_UPDATE,
  // Router Events
  RTC_CAFE_ROUTER_EVENT_MESSAGE,
  // Client Events
  RTC_CAFE_CLIENT_EVENT_PEER_TRACK_DEFINED,
  RTC_CAFE_CLIENT_EVENT_PEER_TRACK_REMOVED,
  RTC_CAFE_CLIENT_EVENT_PEER_TRACK_DEFINITION_REMOVED,
  RTC_CAFE_CLIENT_EVENT_PEER_TRACK_ADDED,
} from "./constants";

const introduceMediaTracks = (tracks, channel, router) => {
  for (const track of tracks) {
    const {
      id,
      description,
      track: { kind },
    } = track;
    router.sendMessage(channel.remoteId, {
      messageType: RTC_MSG_TYPE_INTRODUCE_MEDIA_TRACK,
      message: {
        id,
        description,
        kind,
      },
    });
  }
};

export const handleTrackManagementOverWebRTC = (client, channel, router) => {
  const { triggerHandler, getTracks } = client;
  const boundTriggerHandler = triggerHandler.bind(client);
  const boundGetTracks = getTracks.bind(client);
  const peerId = channel.remoteId;

  introduceMediaTracks(boundGetTracks(), channel, router);

  channel.on(RTC_CAFE_CHANNEL_EVENT_PEER_TRACK_REMOVED, (id) => {
    boundTriggerHandler(RTC_CAFE_CLIENT_EVENT_PEER_TRACK_REMOVED, peerId, id);
  });

  channel.on(
    RTC_CAFE_CHANNEL_EVENT_PEER_TRACK_DEFINITION_REMOVED,
    (id, kind, description) => {
      boundTriggerHandler(
        RTC_CAFE_CLIENT_EVENT_PEER_TRACK_DEFINITION_REMOVED,
        peerId,
        id,
        kind,
        description
      );
    }
  );

  channel.on(RTC_CAFE_CHANNEL_EVENT_PEER_TRACK_IDENTIFIED, (track, id) => {
    boundTriggerHandler(
      RTC_CAFE_CLIENT_EVENT_PEER_TRACK_ADDED,
      peerId,
      id,
      track
    );
  });

  // The local description will be updated after the media track is added
  channel.on(RTC_CAFE_CHANNEL_EVENT_LOCAL_DESCRIPTION_UPDATE, () => {
    // After local description is added, we can begin to correlate
    // the tranceiver "mid" value to the local track id)
    for (const { mid, id } of channel.getSentTrackCorrelations()) {
      router.sendMessage(peerId, {
        messageType: RTC_MSG_TYPE_CORRELATE_MEDIA_TRACK,
        message: { id, mid },
      });
    }
  });

  channel.on(
    RTC_CAFE_CHANNEL_EVENT_PEER_TRACK_DEFINED,
    ({ id, description, kind }) => {
      boundTriggerHandler(RTC_CAFE_CLIENT_EVENT_PEER_TRACK_DEFINED, peerId, {
        id,
        description,
        kind,
      });
    }
  );

  channel.peerTrackRequestFunction = (id) => {
    router.sendMessage(peerId, {
      messageType: RTC_MSG_TYPE_REQUEST_MEDIA_TRACK,
      message: { id },
    });
  };

  channel.removeTrackNotificationFunction = (id) => {
    router.sendMessage(peerId, {
      messageType: RTC_MSG_TYPE_REMOVE_MEDIA_TRACK,
      message: { id },
    });
  };

  router.on(
    `${RTC_CAFE_ROUTER_EVENT_MESSAGE}.originalSender:${peerId}.sender:${peerId}`,
    (json) => {
      const { messageType } = json;
      if (messageType === RTC_MSG_TYPE_INTRODUCE_MEDIA_TRACK) {
        const {
          message: { id, description, kind },
        } = json;
        channel.definePeerTrack(id, description, kind);
      } else if (messageType === RTC_MSG_TYPE_REMOVE_MEDIA_TRACK) {
        const {
          message: { id },
        } = json;
        channel.removePeerTrackDefinition(id);
      } else if (messageType === RTC_MSG_TYPE_REQUEST_MEDIA_TRACK) {
        const {
          message: { id },
        } = json;
        const tracks = boundGetTracks([id]);
        if (tracks.length === 0) {
          throw new Error(
            `Got request from "${peerId}" for non-existent media track "${id}"`
          );
        }
        const [{ track, description }] = tracks;
        channel.addTrack(track, description);
      } else if (messageType === RTC_MSG_TYPE_CORRELATE_MEDIA_TRACK) {
        const {
          message: { mid, id },
        } = json;
        channel.correlatePeerMediaTrack(id, mid);
      }
    }
  );
};
