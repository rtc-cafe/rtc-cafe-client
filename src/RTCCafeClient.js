import { v4 as uuidv4 } from "uuid";

import {
  LATEST_API_VERSION,
  RTC_MSG_TYPE_USER_MSG,
  RTC_MSG_TYPE_REASSIGN_SID,
  RTC_MSG_TYPE_INTRODUCE_MEDIA_TRACK,
  // Signaller Events
  RTC_CAFE_SIGNALLING_CLIENT_EVENT_MESSAGE,
  // Router Events
  RTC_CAFE_ROUTER_EVENT_MESSAGE,
  RTC_CAFE_ROUTER_EVENT_REACHABILITY_UPDATED,
  RTC_CAFE_ROUTER_EVENT_REACHABILITY_BULK_UPDATED,
  // Client Events
  RTC_CAFE_CLIENT_EVENT_ACQUIRE_SID,
  RTC_CAFE_CLIENT_EVENT_SIGNALLER_STATE_CHANGED,
  RTC_CAFE_CLIENT_EVENT_MESSAGE,
  RTC_CAFE_CLIENT_EVENT_REASSIGN_PEER_SID,
  RTC_CAFE_CLIENT_EVENT_DISCONNECT_FROM_PEER,
  RTC_CAFE_CLIENT_EVENT_CONNECT_TO_PEER,
  RTC_CAFE_CLIENT_EVENT_SIGNALLER_ERROR,
} from "./constants";
import { startPeerDiscovery } from "./peer-discovery";
import {
  initiateHandshakeOverSignaller,
  handleOffersOverSignaller,
} from "./signaller-handshakes";
import {
  handleRenegotiationOverWebRTC,
  handleOffersOverWebRTC,
  initIntroHandshakeOverWebRTC,
} from "./webrtc-handshake";
import { handleTrackManagementOverWebRTC } from "./media-management";
import { OrderedDict } from "./OrderedDict";
import { RTCCafeChannelPool } from "./RTCCafeChannelPool";
import { RTCCafeRouter } from "./RTCCafeRouter";
import { RTCCafeSignallingClient } from "./RTCCafeSignallingClient";

const setChannel = (client, channel) => {
  const { triggerHandler, _router, _discoveredPeers } = client;
  const boundTriggerHandler = triggerHandler.bind(client);
  const { remoteId } = channel;
  const existingChannel = _router.get(remoteId);
  if (existingChannel) {
    // We may have created two channels if both peers started peer discovery simultaniously
    existingChannel.disconnect();
  }
  channel.off();
  _router.addChannel(channel);
  handleTrackManagementOverWebRTC(client, channel, _router);
  handleRenegotiationOverWebRTC(channel, _router);

  channel.on("dataChannelClose", () => {
    const { remoteId: curRemoteId } = channel;
    _router.removeChannel(curRemoteId);
    _discoveredPeers.remove(curRemoteId);
    boundTriggerHandler(
      RTC_CAFE_CLIENT_EVENT_DISCONNECT_FROM_PEER,
      curRemoteId
    );
  });

  client.triggerHandler(RTC_CAFE_CLIENT_EVENT_CONNECT_TO_PEER, remoteId);
};

const consumePeersToHandshake = (client) => {
  const {
    _router,
    _discoveredPeers,
    _peersToHandshake,
    _handshakeInProgress,
    _enableWebRTCHandshake,
  } = client;
  while (_peersToHandshake.length > 0) {
    if (client.state !== "connected") {
      return;
    }

    const [{ id }] = _peersToHandshake.splice(0, 1);
    const isPeerReachable = _router.isPeerReachable(id);

    if (_router.channelIds.includes(id)) {
      return;
    }

    if (
      _enableWebRTCHandshake &&
      !isPeerReachable &&
      _handshakeInProgress.keys.length > 0
    ) {
      // Peer is not reachable, but when the current handshakes have completed, it might be.
      // Prepend the peer back on the queue until other handshakes have completed
      _peersToHandshake.splice(0, 0, { id });
      break;
    }

    if (!_enableWebRTCHandshake || !isPeerReachable) {
      // We have to do handshake over signaller because either:
      // 1) There are no other channels we can do handshake over
      // 2) Intro handshakes over webrtc are disabled
      _handshakeInProgress.set(id, true);
      initiateHandshakeOverSignaller(client, id)
        .then((channel) => setChannel(client, channel))
        .catch((error) => {
          _handshakeInProgress.remove(id);
          if (error.message === "signallerConnectionLost") {
            // We lost the signaller connection while handshake was in progress
            _discoveredPeers.remove(id);
          } else {
            // Handshake failed for some other reason. Retry
            _peersToHandshake.splice(0, 0, { id });
          }
          consumePeersToHandshake(client);
        });
      continue;
    }

    // Hanshake over webrtc is enabled and peer is reachable via webrtc
    _handshakeInProgress.set(id, true);
    initIntroHandshakeOverWebRTC(client, _router, id)
      .then((channel) => setChannel(client, channel))
      .catch(() => {
        // The peer became unreachable while handshake was in progress
        _handshakeInProgress.remove(id);
        _peersToHandshake.splice(0, 0, { id });
        consumePeersToHandshake(client);
      });
  }
};

const pushPeerToHandshake = (client, id) => {
  const { _peersToHandshake } = client;
  _peersToHandshake.push({ id });
  consumePeersToHandshake(client);
};

const startPeerDiscoveryIfReady = (client) => {
  if (client.sid === "unassigned" || !client.room) {
    return;
  }
  const { _discoveredPeers, _signallerSidLookup } = client;
  handleOffersOverSignaller(
    client,
    // setPeerSid
    (id, sid) => {
      _signallerSidLookup.set(id, sid);
      _discoveredPeers.set(id, true);
    },
    // setChannel
    (channel) => setChannel(client, channel)
  );
  startPeerDiscovery(
    client,
    // setDiscoveredPeer
    (id, sid) => {
      // We will initiate the connection with this peer
      _signallerSidLookup.set(id, sid);
      _discoveredPeers.set(id, true);
    },
    // initiateHandshake
    (id) => pushPeerToHandshake(client, id)
  );
};

export class RTCCafeClient {
  constructor(rtcConfig, options = {}) {
    this.rtcConfig = rtcConfig;
    this.apiVersion = options.apiVersion || LATEST_API_VERSION;
    if (
      !Number.isInteger(this.apiVersion) ||
      this.apiVersion > LATEST_API_VERSION ||
      this.apiVersion < 1
    ) {
      throw new Error(`Invalid API Version ${this.apiVersion}`);
    }

    options = {
      enableWebRTCHandshake: true,
      apiVersion: LATEST_API_VERSION,
      ...options,
    };

    if (options.signaller) {
      this.signaller = options.signaller;
      if (this.signaller.apiVersion !== this.apiVersion) {
        throw new Error(
          `Signaller API Version ${this.signaller.apiVersion} does not match ` +
            `Client API Version ${this.apiVersion}.`
        );
      }
      if (!(this.signaller instanceof RTCCafeSignallingClient)) {
        throw new Error('"signaller" is not RTCCafeSignallingClient');
      }
    } else {
      this.signaller = new RTCCafeSignallingClient({
        apiVersion: this.apiVersion,
      });
    }

    this._permanentId = uuidv4();
    this.state = "setup";
    this.sid = "unassigned";
    this._enableWebRTCHandshake = options.enableWebRTCHandshake;
    this._handlers = {};
    this._channelPool = new RTCCafeChannelPool();
    this._router = new RTCCafeRouter(this._permanentId);
    this._discoveredPeers = new OrderedDict();
    this._tracks = new OrderedDict();
    this._signallerSidLookup = new OrderedDict();
    this._handshakeInProgress = new OrderedDict();
    this._peersToHandshake = [];

    const acquireSid = () => {
      this.sid = this.signaller.sid;
      this.triggerHandler(RTC_CAFE_CLIENT_EVENT_ACQUIRE_SID, this.sid);
    };

    if (this.signaller.state === "connected") {
      this.state = "connected";
      acquireSid();
      startPeerDiscoveryIfReady(this);
      consumePeersToHandshake(this);
    }

    this.signaller.on("stateChanged", (state) => {
      this.state = state;
      this.triggerHandler(
        RTC_CAFE_CLIENT_EVENT_SIGNALLER_STATE_CHANGED,
        this.state
      );
      if (state === "connected") {
        const oldSid = this.sid === "unassigned" ? null : this.sid;
        acquireSid();
        if (oldSid) {
          this._router.broadcast({
            messageType: RTC_MSG_TYPE_REASSIGN_SID,
            message: {
              newSid: this.sid,
            },
          });
          consumePeersToHandshake(this);
        }
        startPeerDiscoveryIfReady(this);
      }
    });

    this.signaller.on(
      `${RTC_CAFE_SIGNALLING_CLIENT_EVENT_MESSAGE}.error`,
      ({ error, originalMessage }) => {
        this.triggerHandler(
          RTC_CAFE_CLIENT_EVENT_SIGNALLER_ERROR,
          error,
          originalMessage
        );
      }
    );

    this._router.on(
      RTC_CAFE_ROUTER_EVENT_MESSAGE,
      (sender, originalSender, json) => {
        const { messageType, message } = json;
        if (messageType === RTC_MSG_TYPE_USER_MSG) {
          this.triggerHandler(RTC_CAFE_CLIENT_EVENT_MESSAGE, sender, message);
        } else if (messageType === RTC_MSG_TYPE_REASSIGN_SID) {
          const { newSid } = message;
          this._signallerSidLookup.set(sender, newSid);
          this.triggerHandler(
            RTC_CAFE_CLIENT_EVENT_REASSIGN_PEER_SID,
            sender,
            newSid
          );
        }
      }
    );
    this._router.on(RTC_CAFE_ROUTER_EVENT_REACHABILITY_UPDATED, () =>
      consumePeersToHandshake(this)
    );
    this._router.on(
      RTC_CAFE_ROUTER_EVENT_REACHABILITY_BULK_UPDATED,
      (remoteId) => {
        this._handshakeInProgress.remove(remoteId);
        consumePeersToHandshake(this);
      }
    );

    handleOffersOverWebRTC(
      this,
      this._router,
      // setChannel
      (channel) => setChannel(this, channel)
    );
  }

  async triggerHandler(name, ...args) {
    let handlers = this._handlers[name] || [];
    handlers = [...handlers];
    await Promise.all(handlers.map((handler) => handler(...args)));
  }

  joinRoom(room) {
    if (this.room) {
      throw new Error("Already in/joining room");
    }
    this.room = room;
    startPeerDiscoveryIfReady(this);
  }

  on(eventName, handler) {
    this._handlers[eventName] = this._handlers[eventName] || [];
    this._handlers[eventName].push(handler);
  }

  off(eventName, handler) {
    if (!eventName) {
      this._handlers = {};
      return;
    }
    const handlers = this._handlers[eventName];
    if (!handlers || !handlers.includes(handler)) {
      return;
    }
    const index = handlers.indexOf(handler);
    handlers.splice(index, 1);
  }

  addTrack(track, description = "no description") {
    const { id, kind } = track;
    this._tracks.set(id, { description, track, kind });
    this._router.broadcast({
      messageType: RTC_MSG_TYPE_INTRODUCE_MEDIA_TRACK,
      message: {
        id,
        description,
        kind,
      },
    });
    return id;
  }

  removeTrack(id) {
    if (!this._tracks.keys.includes(id)) {
      return null;
    }
    this._tracks.remove(id);
    for (const channel of this._router.channels) {
      if (channel.getTracks([id]).length === 0) {
        continue;
      }
      channel.removeTrack(id);
    }
    return id;
  }

  getTracks(idFilter) {
    const tracks = [];
    for (const id of this._tracks.keys) {
      if (idFilter && !idFilter.includes(id)) {
        continue;
      }
      const { description, track, kind } = this._tracks[id];
      tracks.push({
        id,
        description,
        kind,
        track,
      });
    }
    return tracks;
  }

  requestPeerTrack(peerId, trackId) {
    const channel = this._router.get(peerId);
    if (channel.isPeerTrackRequested(trackId)) {
      return;
    }
    channel.requestPeerTracks(trackId);
  }

  removePeerTrack(peerId, trackId) {
    return this._router.get(peerId).removePeerTrack(trackId);
  }

  getPeerTrackDefinitions(peerIdFilter, trackIdFilter) {
    const trackDefs = {};
    for (const peerId of this._router.channelIds) {
      if (peerIdFilter && !peerIdFilter.includes(peerId)) {
        continue;
      }
      const tracksForPeer = this._router
        .get(peerId)
        .getPeerTrackDefinitions(trackIdFilter);
      if (Object.keys(tracksForPeer).length > 0) {
        trackDefs[peerId] = tracksForPeer;
      }
    }
    return trackDefs;
  }

  getPeerTracks(peerIdFilter, trackIdFilter) {
    const tracks = {};
    for (const peerId of this._router.channelIds) {
      if (peerIdFilter && !peerIdFilter.includes(peerId)) {
        continue;
      }
      const tracksForPeer = this._router
        .get(peerId)
        .getPeerTracks(trackIdFilter);
      if (Object.keys(tracksForPeer).length > 0) {
        tracks[peerId] = tracksForPeer;
      }
    }
    return tracks;
  }

  getPeerSid(id) {
    return this._signallerSidLookup[id];
  }

  disconnectSignaller() {
    this.signaller.disconnect();
    this.signaller = null;
  }

  disconnectFromPeers() {
    const { channels } = this._router;
    this._router.removeAllChannels();
    this._signallerSidLookup = new OrderedDict();
    this._discoveredPeers = new OrderedDict();
    for (const channel of channels) {
      channel.disconnect();
    }
  }

  get discoveredPeers() {
    return this._discoveredPeers.keys;
  }

  get connectedPeers() {
    return this._router.channelIds;
  }

  get permanentId() {
    return this._permanentId;
  }

  sendMessage(message, peerId) {
    if (peerId) {
      this._router.sendMessage(peerId, {
        messageType: RTC_MSG_TYPE_USER_MSG,
        message,
      });
    } else {
      this._router.broadcast({
        messageType: RTC_MSG_TYPE_USER_MSG,
        message,
      });
    }
  }
}
