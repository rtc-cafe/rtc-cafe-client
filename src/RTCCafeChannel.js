import adapter from "webrtc-adapter";

import { OrderedDict } from "./OrderedDict";
import {
  RTC_CAFE_CHANNEL_EVENT_NEGOTIATION_NEEDED,
  RTC_CAFE_CHANNEL_EVENT_ICE_CANDIDATE,
  RTC_CAFE_CHANNEL_EVENT_PEER_TRACK_RECEIVED,
  RTC_CAFE_CHANNEL_EVENT_PEER_TRACK_IDENTIFIED,
  RTC_CAFE_CHANNEL_EVENT_LOCAL_DESCRIPTION_UPDATE,
  RTC_CAFE_CHANNEL_EVENT_REMOTE_DESCRIPTION_UPDATE,
  RTC_CAFE_CHANNEL_EVENT_PEER_TRACK_DEFINED,
  RTC_CAFE_CHANNEL_EVENT_PEER_TRACK_REMOVED,
  RTC_CAFE_CHANNEL_EVENT_PEER_TRACK_DEFINITION_REMOVED,
  RTC_CAFE_CHANNEL_EVENT_DATA_CHANNEL_OPEN,
  RTC_CAFE_CHANNEL_EVENT_DATA_CHANNEL_CLOSE,
  RTC_CAFE_CHANNEL_EVENT_CONNECTION_STATE_CHANGE,
  RTC_CAFE_CHANNEL_EVENT_MESSAGE,
} from "./constants";

const identifyTrackIfReady = (channel, mid) => {
  const { triggerHandler, _unidentifiedPeerTracks, _peerTracks } = channel;
  const { track, id } = _unidentifiedPeerTracks[mid];
  if (!id || !track) {
    return;
  }
  if (!(id in _peerTracks)) {
    throw new Error(`Peer track "${id}" not defined`);
  }
  const peerTrack = _peerTracks[id];
  peerTrack.track = track;
  _unidentifiedPeerTracks.remove(mid);
  const boundTriggerHandler = triggerHandler.bind(channel);
  boundTriggerHandler(RTC_CAFE_CHANNEL_EVENT_PEER_TRACK_IDENTIFIED, track, id);
};

const handleDataChannelOpen = (channel, dataChannel) => {
  channel._dataChannel = dataChannel;
  const boundTriggerHandler = channel.triggerHandler.bind(channel);
  boundTriggerHandler(RTC_CAFE_CHANNEL_EVENT_DATA_CHANNEL_OPEN);
  dataChannel.onclose = () => {
    boundTriggerHandler(RTC_CAFE_CHANNEL_EVENT_DATA_CHANNEL_CLOSE);
  };
  dataChannel.onmessage = ({ data }) => {
    const json = JSON.parse(data);
    boundTriggerHandler(RTC_CAFE_CHANNEL_EVENT_MESSAGE, json);
  };
};

export class RTCCafeChannel {
  constructor(localId, remoteId, connection) {
    this.localId = localId;
    this.remoteId = remoteId;
    this._browserInfo = adapter.browserDetails.browser;
    this._connection = connection;
    this._handlers = {};
    this._unidentifiedPeerTracks = new OrderedDict();
    this._peerTracks = new OrderedDict();
    this._tracks = new OrderedDict();

    this._connection.ondatachannel = ({ channel }) =>
      handleDataChannelOpen(this, channel);
    this._connection.onnegotiationneeded = () =>
      this.triggerHandler(RTC_CAFE_CHANNEL_EVENT_NEGOTIATION_NEEDED);
    this._connection.onicecandidate = ({ candidate }) =>
      this.triggerHandler(RTC_CAFE_CHANNEL_EVENT_ICE_CANDIDATE, { candidate });
    this._connection.oniceconnectionstatechange = ({
      target: { iceConnectionState },
    }) =>
      this.triggerHandler(RTC_CAFE_CHANNEL_EVENT_CONNECTION_STATE_CHANGE, {
        iceConnectionState,
      });
    this._connection.ontrack = ({ track }) => {
      const { id: trackId } = track;
      const transceiver = this._connection.getTransceivers().find(
        ({
          receiver: {
            track: { id: transceiverTrackId },
          },
        }) => transceiverTrackId === trackId
      );
      const { mid } = transceiver;
      this.triggerHandler(RTC_CAFE_CHANNEL_EVENT_PEER_TRACK_RECEIVED, track);
      const unidentifiedTrackDef = this._unidentifiedPeerTracks[mid] || {};
      this._unidentifiedPeerTracks.set(mid, { track, ...unidentifiedTrackDef });
      identifyTrackIfReady(this, mid);
    };
  }

  createOffer() {
    return this._connection.createOffer().then((offer) =>
      this._connection
        .setLocalDescription(offer)
        .then(() =>
          this.triggerHandler(
            RTC_CAFE_CHANNEL_EVENT_LOCAL_DESCRIPTION_UPDATE,
            offer
          )
        )
        .then(() => offer)
    );
  }

  handleOffer(description) {
    return this._connection
      .setRemoteDescription(description)
      .then(() =>
        this.triggerHandler(
          RTC_CAFE_CHANNEL_EVENT_REMOTE_DESCRIPTION_UPDATE,
          description
        )
      )
      .then(() =>
        this._connection.createAnswer().then((answer) =>
          this._connection
            .setLocalDescription(answer)
            .then(() =>
              this.triggerHandler(
                RTC_CAFE_CHANNEL_EVENT_LOCAL_DESCRIPTION_UPDATE,
                answer
              )
            )
            .then(() => answer)
        )
      );
  }

  handleAnswer(description) {
    return this._connection
      .setRemoteDescription(description)
      .then(() =>
        this.triggerHandler(
          RTC_CAFE_CHANNEL_EVENT_REMOTE_DESCRIPTION_UPDATE,
          description
        )
      )
      .then(() => description);
  }

  addIceCandidate(candidate) {
    if (
      this._browserInfo !== "firefox" &&
      candidate.address &&
      candidate.address.includes(".local")
    ) {
      // Chrome (and possibly other browsers) resolve mDNS hosts via webrtc.
      // We need a preliminary fetch to cache the host resolution first
      fetch(`${document.location.protocol}//${candidate.address}/`)
        .catch(() => {})
        .then(() => {
          this._connection.addIceCandidate(candidate);
        });
      return;
    }
    this._connection.addIceCandidate(candidate);
  }

  getSentTrackCorrelations() {
    return this._connection
      .getTransceivers()
      .filter(({ mid, direction }) => mid && direction !== "recvonly")
      .map(({ mid, sender: { track: { id } } }) => ({ mid, id }));
  }

  createDataChannel(id) {
    const dataChannel = this._connection.createDataChannel(id);
    dataChannel.onopen = () => handleDataChannelOpen(this, dataChannel);
  }

  addTrack(track, description = "no description") {
    const { id } = track;
    this._tracks.set(id, { track, description });
    this._connection.addTransceiver(track);
  }

  async removeTrack(id) {
    if (!this.removeTrackNotificationFunction) {
      throw new Error(
        "RTCCafeChannel must have removeTrackNotificationFunction set to remove media tracks"
      );
    }
    if (!(id in this._tracks)) {
      return null;
    }
    const sender = this._connection
      .getSenders()
      .filter(({ track }) => track)
      .find(({ track: { id: senderTrackId } }) => senderTrackId === id);
    this._connection.removeTrack(sender);
    delete this._tracks[id];
    await this.removeTrackNotificationFunction(id);
    return id;
  }

  getTracks(idFilter) {
    const tracks = [];
    for (const id of this._tracks.keys) {
      if (idFilter && !idFilter.includes(id)) {
        continue;
      }
      tracks.push(this._tracks[id]);
    }
    return tracks;
  }

  definePeerTrack(id, description, kind) {
    if (id in this._peerTracks) {
      throw new Error(`Peer track "${id}" already defined`);
    }
    this._peerTracks.set(id, { id, description, kind });
    this.triggerHandler(RTC_CAFE_CHANNEL_EVENT_PEER_TRACK_DEFINED, {
      id,
      description,
      kind,
    });
  }

  correlatePeerMediaTrack(id, mid) {
    const unidentifiedTrackDef = this._unidentifiedPeerTracks[mid] || {};
    this._unidentifiedPeerTracks.set(mid, { id, ...unidentifiedTrackDef });
    identifyTrackIfReady(this, mid);
  }

  isPeerTrackRequested(id) {
    if (!(id in this._peerTracks)) {
      throw new Error(`Peer track "${id}" not defined`);
    }
    return this._peerTracks[id].requested;
  }

  async requestPeerTracks(id) {
    if (!this.peerTrackRequestFunction) {
      throw new Error(
        "RTCCafeChannel must have peerTrackRequestFunction set to request media tracks"
      );
    }
    if (!(id in this._peerTracks)) {
      throw new Error(`Peer track "${id}" not defined`);
    }
    const peerTrack = this._peerTracks[id];
    if (peerTrack.requested) {
      throw new Error(`Peer track "${id}" already requested`);
    }
    peerTrack.requested = true;
    await this.peerTrackRequestFunction(id);
  }

  removePeerTrackDefinition(id) {
    const { description, kind } = this._peerTracks[id];
    this._peerTracks.remove(id);
    this.triggerHandler(
      RTC_CAFE_CHANNEL_EVENT_PEER_TRACK_DEFINITION_REMOVED,
      id,
      kind,
      description
    );
    this.triggerHandler(RTC_CAFE_CHANNEL_EVENT_PEER_TRACK_REMOVED, id);
  }

  removePeerTrack(id) {
    const { track } = this._peerTracks[id];
    if (!track) {
      return null;
    }
    const { id: localTrackId } = track;
    const transceiver = this._connection.getTransceivers().find(
      ({
        receiver: {
          track: { id: transceiverTrackId },
        },
      }) => transceiverTrackId === localTrackId
    );
    this._connection.removeTrack(transceiver.sender);
    delete this._peerTracks[id].track;
    delete this._peerTracks[id].requested;
    this.triggerHandler(RTC_CAFE_CHANNEL_EVENT_PEER_TRACK_REMOVED, id);
    return id;
  }

  getPeerTrackDefinitions(idFilter) {
    const tracksDefinitions = {};
    for (const id of this._peerTracks.keys) {
      if (idFilter && !idFilter.includes(id)) {
        continue;
      }
      const { description, kind, requested } = this._peerTracks[id];
      tracksDefinitions[id] = {
        id,
        description,
        kind,
        requested,
      };
    }
    return tracksDefinitions;
  }

  getPeerTracks(idFilter) {
    const tracks = {};
    for (const id of this._peerTracks.keys) {
      if (!this._peerTracks[id].track) {
        continue;
      }
      if (idFilter && !idFilter.includes(id)) {
        continue;
      }
      tracks[id] = this._peerTracks[id].track;
    }
    return tracks;
  }

  sendMessage(message) {
    this._dataChannel.send(JSON.stringify(message));
  }

  disconnect() {
    this._dataChannel.close();
  }

  getBrowserInfo() {
    return this._browserInfo;
  }

  async triggerHandler(name, ...args) {
    let handlers = this._handlers[name] || [];
    handlers = [...handlers];
    await Promise.all(handlers.map((handler) => handler(...args)));
  }

  on(eventName, handler) {
    this._handlers[eventName] = this._handlers[eventName] || [];
    this._handlers[eventName].push(handler);
  }

  off(eventName, handler) {
    if (!eventName) {
      this._handlers = {};
      return;
    }
    const handlers = this._handlers[eventName];
    if (!handlers || !handlers.includes(handler)) {
      return;
    }
    const index = handlers.indexOf(handler);
    handlers.splice(index, 1);
  }
}
