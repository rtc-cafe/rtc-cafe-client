import io from "socket.io-client";

import {
  LATEST_API_VERSION,
  SIGNALLER_EVENTS_FOR_API_VERSION,
  // Events
  RTC_CAFE_SIGNALLING_CLIENT_EVENT_STATE_CHANGED,
  RTC_CAFE_SIGNALLING_CLIENT_EVENT_MESSAGE,
  RTC_CAFE_SIGNALLING_CLIENT_SID,
} from "./constants";

export class RTCCafeSignallingClient {
  constructor({ apiVersion, host, signalModifier, namespace, ioOptions }) {
    ioOptions = { path: "/socket.io", transports: ["websocket"], ...ioOptions };
    host = host || document.location.origin.replace(/^http(s?):/, "ws$1:");
    if (namespace === undefined) {
      namespace = "rtc-cafe";
    }
    if (namespace !== "") {
      host = `${host}/${namespace}`;
    }
    this._handlers = {};
    this._socket = io(host, ioOptions);
    this.apiVersion = apiVersion || LATEST_API_VERSION;
    this.signalModifier = signalModifier;
    this.sid = "unassigned";
    this.state = "setup";

    this._socket.on("connect", () => {
      this.sendSignal("request-sid", { apiVersion: this.apiVersion });
    });

    this._socket.on("sid", ({ sid }) => {
      this.state = "connected";
      this.sid = sid;
      this.triggerHandler(
        RTC_CAFE_SIGNALLING_CLIENT_EVENT_STATE_CHANGED,
        "connected"
      );
    });

    this._socket.on("disconnect", (reason) => {
      if (
        ["ping timeout", "transport close", "transport error"].includes(reason)
      ) {
        // We lost our connection to the server
        // We'll reconnect automatically and get a new sid
        this.state = "reconnecting";
        this.sid = "unassigned";
        this.triggerHandler(
          RTC_CAFE_SIGNALLING_CLIENT_EVENT_STATE_CHANGED,
          "reconnecting"
        );
      }
    });

    const triggerMessageHandler = (event, ...args) => {
      const [{ sid }] = args;
      this.triggerHandler(
        RTC_CAFE_SIGNALLING_CLIENT_EVENT_MESSAGE,
        event,
        ...args
      );
      this.triggerHandler(
        `${RTC_CAFE_SIGNALLING_CLIENT_EVENT_MESSAGE}.${event}`,
        ...args
      );
      if (sid) {
        this.triggerHandler(
          `${RTC_CAFE_SIGNALLING_CLIENT_EVENT_MESSAGE}.${sid}`,
          event,
          ...args
        );
        this.triggerHandler(
          `${RTC_CAFE_SIGNALLING_CLIENT_EVENT_MESSAGE}.${event}.${sid}`,
          ...args
        );
      }
    };

    this._socket.on("sid", (...args) =>
      triggerMessageHandler(RTC_CAFE_SIGNALLING_CLIENT_SID, ...args)
    );
    for (const event of SIGNALLER_EVENTS_FOR_API_VERSION[this.apiVersion]) {
      this._socket.on(event, (...args) =>
        triggerMessageHandler(event, ...args)
      );
    }
  }

  async triggerHandler(name, ...args) {
    let handlers = this._handlers[name] || [];
    handlers = [...handlers];
    await Promise.all(handlers.map((handler) => handler(...args)));
  }

  on(eventName, handler) {
    this._handlers[eventName] = this._handlers[eventName] || [];
    this._handlers[eventName].push(handler);
  }

  off(eventName, handler) {
    if (!eventName) {
      this._handlers = {};
      return;
    }
    const handlers = this._handlers[eventName];
    if (!handlers || !handlers.includes(handler)) {
      return;
    }
    const index = handlers.indexOf(handler);
    handlers.splice(index, 1);
  }

  sendSignal(signalName, data) {
    if (this.signalModifier) {
      data = this.signalModifier(signalName, data);
    }
    this._socket.emit(signalName, data);
  }

  disconnect() {
    this.state = "closed";
    this.triggerHandler(
      RTC_CAFE_SIGNALLING_CLIENT_EVENT_STATE_CHANGED,
      "closed"
    );
    this._socket.close();
  }
}
